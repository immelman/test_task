#!/usr/bin/env python

import csv
from dateutil import parser
import math
import numpy as np

TEST_FILE = 'AMGN-DAY-TICK.CSV'
OUTPUT_FILE = 'result.txt'

TIME_POS = 0
LAST_POS = 1
SIZE_POS = 2

STEP = 100

HOUR_FROM = 9
MINUTE_FROM = 30
HOUR_TO = 16
MINUTE_TO = 0


def get_data_from_csv(file_obj):
    reader = csv.reader(file_obj)
    return list(reader)[1:-1]


def get_min_max(data):
    min = float(data[0][LAST_POS])
    max = float(data[0][LAST_POS])

    for row in data:
        current_value = float(row[LAST_POS])
        if  current_value < min:
            min = current_value
        if current_value > max:
            max = current_value

    return (int(np.floor(min) * 100), int(float(max) * 100))


def sum_last_size(data, time_scale, price_scale):
    result = np.zeros((len(price_scale), len(time_scale)))

    for row in data:
        dt = parser.parse(row[TIME_POS])
        if dt.hour == HOUR_FROM and dt.minute >= MINUTE_FROM\
                or (HOUR_FROM + 1) <= dt.hour < HOUR_TO:
            try:
                x_pos = get_interval_pos(dt.hour, time_scale, time_scale=True)
                y_pos = \
                    len(price_scale) - \
                    get_interval_pos(
                        float(row[LAST_POS]) * 100, price_scale
                    )
                result[y_pos, x_pos] = result[y_pos, x_pos] + float(row[SIZE_POS])
            except ValueError:
                print(10)
                pass

    return result


def get_interval_pos(value, list, time_scale=False):
    if time_scale:
        if value < list[0] or value > list[-1]:
            raise ValueError
    else:
        if value <= list[0] or value > list[-1]:
            raise ValueError
    result = 0
    while result < len(list):
        if value <= list[result]:
            return result
        else:
            result += 1
    return result


def get_price_scale(min, max, step):
    result = []

    min = int(min - step/2)

    result.append(min)
    while min <= max:
        min += step
        result.append(min)

    return result


def save_data(data, time_scale, price_scale):
    y_pos = 1
    with open(OUTPUT_FILE, "w") as file:
        for row in data:
            for value in row:
                low_cluster = price_scale[len(price_scale) - y_pos]
                cluster_id = low_cluster + STEP/2
                high_cluster = low_cluster + STEP
                file.write('{0:7d}\t'.format(int(value)))
            file.writelines(
                '\t{} - ({};{}]\n'.format(cluster_id/100, low_cluster/100, high_cluster/100))
            y_pos += 1

        for item in time_scale:
            file.write('{0:7d}\t'.format(item))

        file.write('\n')


def main():
    with open(TEST_FILE, "r") as file:
        data = get_data_from_csv(file)

    (min, max) = get_min_max(data)

    price_scale = get_price_scale(min, max, STEP)

    time_scale = [HOUR_FROM + value for value in range(HOUR_TO - HOUR_FROM)]

    result = sum_last_size(data, time_scale, price_scale)
    save_data(result, time_scale, price_scale)


if __name__ == "__main__":
    main()
